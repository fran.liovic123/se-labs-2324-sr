def char_types_count(input_string):
    uppercase_count = 0
    lowercase_count = 0
    number_count = 0

    for char in input_string:
        if char.isupper():
            uppercase_count += 1
        elif char.islower():
            lowercase_count += 1
        elif char.isdigit():
            number_count += 1

    return uppercase_count, lowercase_count, number_count

input_string = "asdf98CXX21grrr"
result = char_types_count(input_string)
print(result)  