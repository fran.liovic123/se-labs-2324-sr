from query_handler_base import QueryHandlerBase
import random
import requests
import json

class DadJokeHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "dad" in query:
            return True
        return False

    def process(self, query):
        try:
            result = self.call_api()
            txt = result["joke"]
            self.ui.say(txt)
        except Exception as e: 
            self.ui.say("Oh no! There was an error.")
            self.ui.say("Try something else!")



    def call_api(self):
        url = "https://daddyjokes.p.rapidapi.com/random"

        headers = {
                "X-RapidAPI-Key": "1fd8c5480emsh78a8bdca81cd451p116927jsn441c120277f9",
	            "X-RapidAPI-Host": "daddyjokes.p.rapidapi.com"
        }

        response =  requests.get(url, headers=headers)

        return json.loads(response.text)